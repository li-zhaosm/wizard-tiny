package com.linyingapp.wizard.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 定时任务配置
 *
 * @date: 2022/6/27 15:56
 * @author: linying
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
