package com.linyingapp.wizard.modules.ums.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linyingapp.wizard.modules.ums.model.UmsRoleResourceRelation;

/**
 * 角色资源关系管理Service
 * @author ly
 */
public interface UmsRoleResourceRelationService extends IService<UmsRoleResourceRelation> {
}
