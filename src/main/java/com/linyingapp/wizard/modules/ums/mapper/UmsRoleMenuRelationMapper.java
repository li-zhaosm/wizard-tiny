package com.linyingapp.wizard.modules.ums.mapper;

import com.linyingapp.wizard.modules.ums.model.UmsRoleMenuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台角色菜单关系表 Mapper 接口
 * </p>
 *
 * @author linying
 * @since 2020-08-21
 */
public interface UmsRoleMenuRelationMapper extends BaseMapper<UmsRoleMenuRelation> {

}
