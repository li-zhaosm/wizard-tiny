package com.linyingapp.wizard.modules.ums.mapper;

import com.linyingapp.wizard.modules.ums.model.UmsResourceCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源分类表 Mapper 接口
 * </p>
 *
 * @author linying
 * @since 2020-08-21
 */
public interface UmsResourceCategoryMapper extends BaseMapper<UmsResourceCategory> {

}
