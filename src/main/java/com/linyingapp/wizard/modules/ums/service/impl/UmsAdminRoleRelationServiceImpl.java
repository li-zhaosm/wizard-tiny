package com.linyingapp.wizard.modules.ums.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.linyingapp.wizard.modules.ums.service.UmsAdminRoleRelationService;
import com.linyingapp.wizard.modules.ums.mapper.UmsAdminRoleRelationMapper;
import com.linyingapp.wizard.modules.ums.model.UmsAdminRoleRelation;
import org.springframework.stereotype.Service;

/**
 * 管理员角色关系管理Service实现类
 */
@Service
public class UmsAdminRoleRelationServiceImpl extends ServiceImpl<UmsAdminRoleRelationMapper, UmsAdminRoleRelation> implements UmsAdminRoleRelationService {
}
