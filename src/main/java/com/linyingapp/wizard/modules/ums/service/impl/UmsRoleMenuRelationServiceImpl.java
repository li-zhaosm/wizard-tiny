package com.linyingapp.wizard.modules.ums.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.linyingapp.wizard.modules.ums.mapper.UmsRoleMenuRelationMapper;
import com.linyingapp.wizard.modules.ums.model.UmsRoleMenuRelation;
import com.linyingapp.wizard.modules.ums.service.UmsRoleMenuRelationService;
import org.springframework.stereotype.Service;

/**
 * 角色菜单关系管理Service实现类
 * @author ly
 */
@Service
public class UmsRoleMenuRelationServiceImpl extends ServiceImpl<UmsRoleMenuRelationMapper, UmsRoleMenuRelation> implements UmsRoleMenuRelationService {
}
