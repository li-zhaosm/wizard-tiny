package com.linyingapp.wizard.modules.ums.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.linyingapp.wizard.modules.ums.service.UmsRoleResourceRelationService;
import com.linyingapp.wizard.modules.ums.mapper.UmsRoleResourceRelationMapper;
import com.linyingapp.wizard.modules.ums.model.UmsRoleResourceRelation;
import org.springframework.stereotype.Service;

/**
 * 角色资源关系管理Service实现类
 */
@Service
public class UmsRoleResourceRelationServiceImpl extends ServiceImpl<UmsRoleResourceRelationMapper, UmsRoleResourceRelation> implements UmsRoleResourceRelationService {
}
